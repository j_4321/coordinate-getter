coordinate-getter - Get point coordinates on a picture
======================================================
|Linux| |License|

Get point coordinates on a picture.


Install
-------

.. ~- Archlinux

.. ~    coordinate-getter is available in `AUR <https://aur.archlinux.org/packages/coordinate-getter>`_.

.. ~- Ubuntu

.. ~    coordinate-getter is available in the PPA `ppa:j-4321-i/ppa <https://launchpad.net/~j-4321-i/+archive/ubuntu/ppa>`_.

.. ~    ::

.. ~        $ sudo add-apt-repository ppa:j-4321-i/ppa
.. ~        $ sudo apt-get update
.. ~        $ sudo apt-get install coordinate-getter

- Source code

    First, install the missing dependencies among:

        - Python 3
        - Numpy 
        - Pillow

    Then install the application:

    ::

        $ sudo python3 setup.py install


You can now launch it from *Menu > CATEGORY > coordinate-getter*. You can launch
it from the command line with ``coordinate-getter``.


Troubleshooting
---------------

If you encounter bugs or if you have suggestions, please open an issue
on `Gitlab <https://gitlab.com/j_4321/coordinate-getter/issues>`_.

.. ~.. |Release| image:: https://badge.fury.io/gh/j4321%2Fcoordinate-getter.svg
.. ~    :alt: Latest Release
    :target: https://badge.fury.io/gh/j4321%2Fcoordinate-getter
.. |Linux| image:: https://img.shields.io/badge/platform-Linux-blue.svg
    :alt: Linux
.. |License| image:: https://img.shields.io/github/license/j4321/coordinate-getter.svg
    :target: https://www.gnu.org/licenses/gpl-3.0.en.html
    :alt: License - GPLv3
