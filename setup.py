#! /usr/bin/python
# -*- coding:Utf-8 -*-

from setuptools import setup
from coordinategetterlib.constants import APP_NAME

import os
import sys

with open('README.rst', encoding='utf-8') as f:
    long_description = f.read()


with open("coordinategetterlib/__init__.py") as file:
    exec(file.read())

if 'linux' in sys.platform:
    images = [os.path.join("coordinategetterlib/images/", img) for img in os.listdir("coordinategetterlib/images/")]
    data_files = [("/usr/share/applications", ["{}.desktop".format(APP_NAME)]),
                  ("/usr/share/{}/images/".format(APP_NAME), images),
                  ("/usr/share/doc/{}/".format(APP_NAME), ["README.rst"]),
                  ("/usr/share/man/man1", ["{}.1.gz".format(APP_NAME)]),
                  ("/usr/share/pixmaps", ["{}.svg".format(APP_NAME)])]
    for loc in os.listdir('coordinategetterlib/locale'):
        data_files.append(("/usr/share/locale/{}/LC_MESSAGES/".format(loc),
                           ["coordinategetterlib/locale/{}/LC_MESSAGES/{}.mo".format(loc, APP_NAME)]))
    print(data_files)
    package_data = {}
else:
    data_files = []
    data = ['images/*']
    for loc in os.listdir('coordinategetterlib/locale'):
        data.append("coordinategetterlib/locale/{}/LC_MESSAGES/{}.mo".format(loc, APP_NAME))
    package_data = {'coordinategetterlib': data}


setup(name=APP_NAME,
      version=__version__,
      description="Get point coordinates on a picture",
      author="Juliette Monsel",
      author_email="j_4321@protonmail.com",
      license="GPLv3",
      url="https://github.com/j4321/{}/".format(APP_NAME),
      packages=['coordinategetterlib'],
      scripts=[APP_NAME],
      data_files=data_files,
      package_data=package_data,
      classifiers=[
              'Development Status :: 5 - Production/Stable',
              # 'Intended Audience :: Developers',
              # 'Intended Audience :: End Users/Desktop',
              # 'Operating System :: OS Independent',
              # 'Topic :: ...',
              'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
              'Programming Language :: Python :: 3',
              'Programming Language :: Python :: 3.5',
              'Programming Language :: Python :: 3.6',
              'Natural Language :: English',
              'Natural Language :: French'
      ],
      long_description=long_description,
      install_requires=["Pillow", "numpy"])
