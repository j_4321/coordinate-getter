# -*- coding: utf-8 -*-
"""
coordinate-getter - Get point coordinates on a picture
Copyright 2021 Juliette Monsel <j_4321@protonmail.com>

coordinate-getter is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

coordinate-getter is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Help dialog
"""
from tkinter import Text, Toplevel
from tkinter.ttk import Button
from webbrowser import open as url_open

from .constants import APP_NAME, REPORT_URL
from .autoscrollbar import AutoScrollbar


class Help(Toplevel):
    """Help Toplevel."""

    def __init__(self, master):
        """Create the Help Toplevel."""
        Toplevel.__init__(self, master)
        self.columnconfigure(0, weight=1)
        self.rowconfigure(0, weight=1)
        self.title(_("Help"))
        text = Text(self, tabs=(2, 2), wrap="word", width=70, height=36, padx=4, pady=4)
        scroll = AutoScrollbar(self, orient='vertical', command=text.yview)
        text.tag_configure('title', font='TkDefaultFont 12 bold')
        text.tag_configure('italic', font='TkDefaultFont 9 italic')
        text.tag_configure('bold', font='TkDefaultFont 9 bold')
        text.tag_configure('title2', font='TkDefaultFont 10 bold', spacing3=10, spacing1=10)
        text.tag_configure('list', lmargin2=20, spacing3=2, spacing1=2)
        text.tag_configure('email', font='TkDefaultFont 9 underline', foreground='blue')
        text.tag_configure('url', font='TkDefaultFont 9 underline', foreground='blue')
        text.tag_bind("link", "<Enter>", lambda e: text.configure(cursor='hand1'))
        text.tag_bind("link", "<Leave>", lambda e: text.configure(cursor='arrow'))
        text.tag_bind("url", "<1>", lambda e: url_open(REPORT_URL))
        text.bind('<1>', lambda e: text.focus_set())  # allow pasting from disabled Text widget
        text.image_create("end", image="img_icon")
        text.insert("end", " {app_name}\n".format(app_name=APP_NAME), "title")
        text.insert("end",
                    _("Get point coordinates on a picture") + "\n\n",
                    "italic")
        #~text.insert("end",
        #~            _("Long description.") + "\n\n")
        text.insert("end",
                    _("{app_name} is designed for Linux. It is written in "
                      "Python 3 and relies mostly upon Tk GUI toolkit. ").format(app_name=APP_NAME) + "\n\n")
        text.insert("end", _("Usage") + "\n", "title2")
        #~text.insert("end", _("Help. List of features: ") + "\n")
        text.insert("end",
                    "\t•\t" + _("Use Ctrl+O or the menu to open an image.") + "\n",
                    'list')
        text.insert("end",
                    "\t•\t" + _("Left click to add points on the image.") + "\n",
                    'list')
        text.insert("end",
                    "\t•\t" + _("Maintain the Ctrl key pressed and click to set the x and y scales.") + "\n",
                    'list')
        text.insert("end",
                    "\t•\t" + _("Use Ctrl+E or the menu to export the points as a list in the clipboard.") + "\n",
                    'list')
        text.insert("end",
                    "\t•\t" + _("Use Ctrl+S or the menu to save the points in a .npy or .txt file.") + "\n",
                    'list')
        text.insert("end",
                    "\t•\t" + _("Use Ctrl+Z or the menu to remove the last point.") + "\n",
                    'list')
        text.insert("end",
                    "\t•\t" + _("Use Ctrl+C or the menu to clear the points.") + "\n",
                    'list')
        text.insert("end",
                    "\t•\t" + _("Use Ctrl + mouse wheel or the menu to zoom in and out.") + "\n",
                    'list')
        #~text.insert("end", _("Troubleshooting") + "\n", "title2")
        #~text.insert("end",
        #~            _("Several GUI toolkits are available to display the system tray icon, "
        #~              "so if the icon does not behave properly, try to change toolkit, "
        #~              "they are not all fully compatible with every desktop environment.") + "\n\n")
        #~text.insert("end",
        #~            _("If the widgets disappear when you click on them, "
        #~              "open the setting dialog from the menu and check the box "
        #~              "'Check this box if the widgets disappear when you click'.") + "\n\n")
        #~text.insert("end",
        #~            _("If you encounter bugs or if you have suggestions, please open an issue on "))
        #~text.insert("end", "Github", ("url", "link"))
        #~text.insert("end", ".")
        text.configure(state='disabled', yscrollcommand=scroll.set)
        text.grid(row=0, column=0, sticky='ewns')
        scroll.grid(row=0, column=1, sticky='ns')
        Button(self, text=_("Close"), command=self.exit).grid(row=1, columnspan=2,
                                                              pady=8, padx=8)

        self.protocol("WM_DELETE_WINDOW", self.exit)
        self.grab_set()

    def exit(self):
        if self.master:
            self.master.focus_set()
        self.destroy()
