# -*- coding: utf-8 -*-
"""
coordinate-getter - Get point coordinates on a picture
Copyright 2021 Juliette Monsel <j_4321@protonmail.com>

coordinate-getter is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

coordinate-getter is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.


Main window
"""
import tkinter as tk
from tkinter import ttk
import logging
import traceback
import os

from PIL import Image
from PIL.ImageTk import PhotoImage
import numpy as np

from . import constants as cst
from .constants import IMAGES
from .about import About
from .help import Help
from .messagebox import showinfo, showerror, askokcancel
# from .version_check import UpdateChecker
from .autoscrollbar import AutoScrollbar


class App(tk.Tk):
    def __init__(self, filepath=None):
        tk.Tk.__init__(self, className=cst.APP_NAME)
        logging.info('Starting {}'.format(cst.APP_NAME))

        self.columnconfigure(0, weight=1)
        self.rowconfigure(0, weight=1)

        self.protocol('WM_DELETE_WINDOW', self.quit)

        # --- images
        self._images = {name: tk.PhotoImage(f'img_{name}', file=IMAGES[name], master=self)
                        for name, path in IMAGES.items()}

        self.iconphoto(True, 'img_icon')

        # --- draw var
        self._current_image = ""
        self._pil_img = None
        self._zoomed_img = None
        self._zoom_ratio = 1
        self._img = None
        self._mouse_pos = (0, 0)
        self._points = []
        self._draw_mode = tk.StringVar(self, "points") # other possibilities are "vguide" and "hguide"

        self._undo_stack = []
        self._redo_stack = []
        # guides
        self._vguides = []
        self._hguides = []
        self._xlim = [0, 1]
        self._ylim = [0, 1]
        # measures
        self._measures = [] # [(x0, y0, x1, y1)]
        self._measure_draw = []
        # scale
        self._scale_draw = []
        self._scale_lines = {"x": [], "y": []}
        self._scale = {"x": [1, 0, 0], # [a_x, x0, scaled_x0] with scaled_x = a_x(x - x0) + scaled_x0
                       "y": [1, 0, 0]} # [a_y, y0, scaled_y0] with scaled_y = a_y(y - y0) + scaled_y0

        # --- style
        style = ttk.Style(self)
        style.theme_use('clam')
        bg = style.lookup('TFrame', 'background', default='#ececec')
        style.map("TCombobox",
                  fieldbackground=[('readonly', bg)],
                  selectbackground=[('readonly', bg)],
                  selectforeground=[('readonly', 'black')],
                  foreground=[('readonly', 'black')])
        self.configure(bg=bg)
        self.option_add('*Toplevel.background', bg)
        self.option_add('*Menu.background', bg)
        self.option_add('*Canvas.background', bg)
        self.option_add('*Menu.tearOff', False)

        # --- menu
        menu = tk.Menu(self)

        # --- --- image
        menu_image = tk.Menu(menu)
        menu_image.add_command(label=_("Open"), command=self.open_image,
                               accelerator="Ctrl+O")
        menu_image.add_command(label=_("Clear"), command=self.clear,
                               accelerator="Ctrl+C")

        menu_image.add_command(label=_("Save"), command=self.save,
                               accelerator="Ctrl+S")

        menu_image.add_command(label=_("Export to clipboard"), command=self.export,
                               accelerator="Ctrl+E")

        # --- --- tools
        menu_tools = tk.Menu(menu)
        menu_tools.add_radiobutton(label=_("Points"), variable=self._draw_mode, value="points", command=self.toggle_mode)
        menu_tools.add_radiobutton(label=_("Horizontal guide"), variable=self._draw_mode, value="hguide", command=self.toggle_mode)
        menu_tools.add_radiobutton(label=_("Vertical guide"), variable=self._draw_mode, value="vguide", command=self.toggle_mode)
        menu_tools.add_radiobutton(label=_("Measure"), variable=self._draw_mode, value="measure", command=self.toggle_mode)
        menu_tools.add_radiobutton(label=_("Scale"), variable=self._draw_mode, value="scale", command=self.toggle_mode)
        menu_tools.add_separator()
        menu_tools.add_command(label=_("Undo"), command=self.undo,
                               accelerator="Ctrl+Z")
        menu_tools.add_command(label=_("Redo"), command=self.undo,
                               accelerator="Ctrl+Y")

        # --- --- view
        menu_view = tk.Menu(menu)
        menu_view.add_command(label=_("Zoom in"), command=self.zoom_in)
        menu_view.add_command(label=_("Zoom out"), command=self.zoom_out)
        menu_view.add_command(label=_("Reset view"), command=self.zoom_reset)

        # --- --- language
        self.language = tk.StringVar(self)
        menu_lang = tk.Menu(menu)
        for key, name in cst.LANGUAGES.items():
            menu_lang.add_radiobutton(label=name, value=key, variable=self.language,
                                      command=self.change_language)
        self.language.set(cst.CONFIG.get('General', 'language'))

        # --- --- help
        menu_help = tk.Menu(menu)
        menu_help.add_command(label=_('About'), image='img_about',
                              compound='left', command=self.about)
        menu_help.add_command(label=_('Help'), image='img_help',
                              compound='left', command=self.help)

        menu.add_cascade(label=_('Image'), menu=menu_image)
        menu.add_cascade(label=_('Tools'), menu=menu_tools)
        menu.add_cascade(label=_('View'), menu=menu_view)
        menu.add_cascade(label=_('Language'), menu=menu_lang)
        menu.add_cascade(label=_('Help'), menu=menu_help)

        self.configure(menu=menu)

        # --- canvas
        self.grid_columnconfigure(0, weight=1)
        self.grid_rowconfigure(0, weight=1)
        self.canvas = tk.Canvas(self, cursor="cross")
        scrollx = AutoScrollbar(self, orient="horizontal", command=self.canvas.xview)
        scrolly = AutoScrollbar(self, orient="vertical", command=self.canvas.yview)

        def scrollsetx(*args, **kw):
            x0, y0 = self.canvas.winfo_rootx(), self.canvas.winfo_rooty()
            x, y = self.canvas.winfo_pointerxy()
            self.mouse_pos = self.canvas.canvasx(x - x0), self.canvas.canvasy(y - y0)
            return scrollx.set(*args, **kw)

        def scrollsety(*args, **kw):
            x0, y0 = self.canvas.winfo_rootx(), self.canvas.winfo_rooty()
            x, y = self.canvas.winfo_pointerxy()
            self.mouse_pos = self.canvas.canvasx(x - x0), self.canvas.canvasy(y - y0)
            return scrolly.set(*args, **kw)

        self.canvas.configure(xscrollcommand=scrollsetx, yscrollcommand=scrollsety)
        self.canvas.grid(row=0, column=0, sticky="eswn")
        scrollx.grid(row=1, column=0, sticky="ew")
        scrolly.grid(row=0, column=1, sticky="ns")

        bottom_frame = ttk.Frame(self)
        bottom_frame.grid(row=2, column=0, sticky="ew")
        self.coordinate_label = ttk.Label(bottom_frame, text="x= \ty= ")
        self.coordinate_label.pack(side='left')
        self.zoom_label = ttk.Label(bottom_frame, text="Zoom: 100%")
        self.zoom_label.pack(side="right")


        #~def only_nb(text):
        #~    return text == '' or text.isdigit()

        #~self._only_nb = self.register(only_nb)

        # --- bindings
        self.canvas.bind("<Motion>", self._coords_update)
        self.canvas.bind("<Control-Motion>", lambda ev: self._coords_update(ev, True))
        self.canvas.bind("<ButtonRelease-1>", self.draw)
        # self.canvas.bind("<Control-ButtonRelease-1>", self._scale_start)
        self.bind("<Control-o>", self.open_image)
        self.bind("<Control-s>", self.save)
        self.bind("<Control-e>", self.export)
        self.bind("<Control-c>", self.clear)
        self.bind("<Control-z>", self.undo)
        self.bind("<Control-y>", self.redo)
        # self.bind("<Escape>", self._measure_cancel)
        self.canvas.bind("<4>", lambda ev: self.canvas.yview("scroll", -3, "units"))
        self.canvas.bind("<5>", lambda ev: self.canvas.yview("scroll", 3, "units"))
        self.canvas.bind("<Shift-4>", lambda ev: self.canvas.xview("scroll", -3, "units"))
        self.canvas.bind("<Shift-5>", lambda ev: self.canvas.xview("scroll", 3, "units"))
        self.canvas.bind("<Configure>", self._coords_update)
        self.canvas.bind("<Control-4>", self.zoom_in)
        self.canvas.bind("<Control-5>", self.zoom_out)
        self.canvas.bind("<Configure>", self._resize)

        self.bind_class("TCombobox", "<<ComboboxSelected>>", lambda ev: ev.widget.selection_clear(), add=True)


        if filepath and os.path.exists(filepath):
            self._current_image = filepath
            self.load_image()

        # --- update check
        #~if cst.CONFIG.getboolean("General", "check_update"):
        #~    UpdateChecker(self)


    @property
    def mouse_pos(self):
        return self._mouse_pos

    @mouse_pos.setter
    def mouse_pos(self, xy):
        self._mouse_pos = tuple(xy)
        self.coordinate_label.configure(text="x={:g}\ty={:g}".format(self.scaled_x(xy[0]), self.scaled_y(xy[1])))

    @property
    def zoom_ratio(self):
        return self._zoom_ratio

    @zoom_ratio.setter
    def zoom_ratio(self, r):
        self._zoom_ratio = r
        self.zoom_label.configure(text="Zoom: {:.1f}%".format(r*100))

    def _coords_update(self, event, straight=False):
        x, y = self.canvas.canvasx(event.x), self.canvas.canvasy(event.y)
        self.mouse_pos = x/self._zoom_ratio, y/self._zoom_ratio

        mode = self._draw_mode.get()
        if mode == "hguide":
            self.canvas.coords("tmp_guide", self._xlim[0], y, self._xlim[1], y)
        elif mode == "vguide":
            self.canvas.coords("tmp_guide", x, self._ylim[0], x, self._ylim[1])
        elif mode == "measure":
            if self._measure_draw:
                if straight:
                    if abs(x - self._measure_draw[0]) > abs(y - self._measure_draw[1]):
                        y = self._measure_draw[1]  # horizontal line
                    else:
                        x = self._measure_draw[0]  # vertical line
                self._measure_draw[2] = x
                self._measure_draw[3] = y
                self.canvas.coords("tmp_measure", *self._measure_draw)
        elif mode == "scale":
            if self._scale_draw:
                if straight:
                    if abs(x - self._scale_draw[0]) > abs(y - self._scale_draw[1]):
                        y = self._scale_draw[1]  # horizontal line
                    else:
                        x = self._scale_draw[0]  # vertical line
                self._scale_draw[2] = x
                self._scale_draw[3] = y
                self.canvas.coords("tmp_scale", *self._scale_draw)

    def report_callback_exception(self, *args):
        """Log exceptions."""
        err = "".join(traceback.format_exception(*args))
        showerror(_("Error"), str(args[1]), err, True)
        logging.error(err)

    def undo(self, event=None):
        if not self._undo_stack:
            return
        mode, coords = self._undo_stack.pop()
        self._redo_stack.append((mode, coords))
        if mode == "points":
            del self._points[-1]
            iid = "@{},{}"
        elif mode == "hguide":
            del self._hguides[-1]
            iid = "hguide{}"
        elif mode == "vguide":
            del self._vguides[-1]
            iid = "vguide{}"
        else: # mode == "measure":
            del self._measures[-1]
            iid =  "measure{},{},{},{}"
        self.canvas.delete(iid.format(*coords))

    def redo(self, event=None):
        if not self._redo_stack:
            return
        mode, coords = self._redo_stack.pop()
        self._undo_stack.append((mode, coords))

        if mode == "points":
            self.point_add(*coords)
        elif mode == "hguide":
            self.hguide_add(*coords)
        elif mode == "vguide":
            self.vguide_add(*coords)
        else: # mode == "measure":
            self.measure_add(*coords)

    def about(self):
        About(self)

    def help(self):
        Help(self)

    def quit(self):
        logging.info('Closing {}'.format(cst.APP_NAME))
        self.destroy()

    def change_language(self):
        cst.CONFIG.set('General', 'language', self.language.get())
        cst.save_config()
        showinfo(_("Information"),
                 _("The language setting will take effect after restarting the application"),
                 parent=self)

    # --- draw
    def toggle_mode(self):
        self._redo_stack.clear()
        mode = self._draw_mode.get()
        self._measure_draw .clear()
        self.canvas.delete("tmp")
        if mode == "hguide":
            y = self._mouse_pos[1]*self.zoom_ratio
            self.canvas.create_line(self._xlim[0], y, self._xlim[1], y, fill="cyan", tags= ["tmp_guide", "guides"])
        elif mode == "vguide":
            x = self._mouse_pos[0]*self.zoom_ratio
            self.canvas.create_line(x, self._ylim[0], x, self._ylim[1], fill="cyan", tags= ["tmp_guide", "guides"])

    def draw(self, event):
        """End drawing on B1-Release."""
        if not self._current_image:
            return
        mode = self._draw_mode.get()
        if mode == "points":
            coords = self.mouse_pos
            self.point_add(*coords)

        elif mode == "hguide":
            coords = [self._mouse_pos[1]]
            self.hguide_add(*coords)

        elif mode == "vguide":
            coords = [self._mouse_pos[0]]
            self.vguide_add(*coords)
        elif  mode == "measure":
            if not self._measure_draw:
                self._measure_start(event)
                self._redo_stack.clear()
                return
            else:
                coords = [c/self._zoom_ratio for c in self._measure_draw]
                self.measure_add(*coords)
                self._measure_cancel()
        else:  # mode == "scale"
            if not self._scale_draw:
                self._scale_start(event)
            else:
                self._end_set_scale(event)
            self._redo_stack.clear()
            return
        self._undo_stack.append((mode, coords))
        self.toggle_mode()

    # --- open
    def open_image(self, event=None):
        if self._current_image:
            if not self.clear():
                return
        initialdir, initialfile = os.path.split(self._current_image)
        filename = cst.askopenfilename(".png", initialdir=initialdir,
                                       initialfile=initialfile,
                                       filetypes=[(_("Images"), "*.png|*.jpg|*.jpeg|*.gif"),
                                                  (_("All files"), "*")])

        if filename:
            self._current_image = filename
            self.load_image()

    def load_image(self):
        self.canvas.delete("all")
        self._scale["x"] = [1, 0, 0]
        self._scale["y"] = [1, 0, 0]
        self._pil_img = Image.open(self._current_image)
        self._zoomed_img = self._pil_img
        self.zoom_ratio = 1
        self._img = PhotoImage(self._zoomed_img, master=self)
        self.canvas.create_image(0, 0, anchor="nw", image=self._img, tags="image")
        self.canvas.configure(scrollregion=self.canvas.bbox("image"))

    # --- points
    def scaled_x(self, x):
        a_x, x0, scaled_x0 = self._scale["x"]
        return a_x*(x - x0) + scaled_x0

    def scaled_y(self, y):
        a_y, y0, scaled_y0 = self._scale["y"]
        return a_y*(y - y0) + scaled_y0

    def get_points(self):
        pts = np.array(self._points)
        pts[:, 0] = self.scaled_x(pts[:, 0])
        pts[:, 1] = self.scaled_y(pts[:, 1])
        return pts

    def point_add(self, x0, y0):
        self._points.append((x0, y0))
        iid = f"@{x0},{y0}"
        x, y = x0*self.zoom_ratio, y0*self.zoom_ratio
        self.canvas.create_line(x - 4, y - 4, x + 5, y + 5, fill="red", width=2, tags=["points", iid])
        self.canvas.create_line(x + 5, y - 4, x - 4, y + 5, fill="red", width=2, tags=["points", iid])
        return iid

    def clear(self, event=None):
        if askokcancel(_("Confirmation"),
                       _("Do you really want to clear the canvas?"),
                       parent=self):
            self.canvas.delete("points")
            self.canvas.delete("guides")
            self.canvas.delete("measures")
            self._points.clear()
            self._measures.clear()
            self._vguides.clear()
            self._hguides.clear()
            self._redo_stack.clear()
            self._undo_stack.clear()
            return True
        return False


    def export(self, event=None):
        """Copy scaled points to clipboard."""
        self.clipboard_clear()
        self.clipboard_append(str(self.get_points().tolist()))

    def save(self, event=None):
        initialdir, initialfile = os.path.split(self._current_image)
        filename = cst.asksaveasfilename(".npy", initialdir=initialdir,
                                         filetypes=[("NPY", "*.npy"),
                                                    ("TXT", ".txt"),
                                                    ("All files", "*")])
        if filename:
            pts = self.get_points()
            if filename.endswith(".npy"):
                np.save(filename, pts)
            else:
                np.savetxt(filename, pts)

    # --- zoom
    def _canvas_redraw(self):
        """Redraw points after zoom change."""
        w, h = self._pil_img.size
        self._zoomed_img = self._pil_img.resize((int(w*self.zoom_ratio), int(h*self.zoom_ratio)), Image.LANCZOS)
        self._img = PhotoImage(self._zoomed_img, master=self)
        self.canvas.itemconfigure("image", image=self._img)
        self._measure_cancel()
        self._scale_cancel()
        # points
        for x0, y0 in self._points:
            iid = f"@{x0},{y0}"
            x, y = x0*self.zoom_ratio, y0*self.zoom_ratio
            items = self.canvas.find_withtag(iid)
            self.canvas.coords(items[0], x - 4, y - 4, x + 5, y + 5)
            self.canvas.coords(items[1], x + 5, y - 4, x - 4, y + 5)
        # measures
        for coords in self._measures:
            iid = "measure{},{},{},{}".format(*coords)
            x0, y0, x1, y1 = [c*self._zoom_ratio for c in coords]
            self.canvas.coords("line_"+ iid, x0, y0, x1, y1)
            self.canvas.coords("txt_"+ iid, (x0 + x1)/2, (y0 + y1)/2)
        # scales
        for ax in "xy":
            coords = [c*self.zoom_ratio for c in self._scale_lines[ax]]
            self.canvas.coords(f"{ax}scale", *coords)
        # guides
        self._guide_redraw()

        self.canvas.configure(scrollregion=self.canvas.bbox("image"))

    def zoom_in(self, event=None):
        if self._pil_img is None or self.zoom_ratio == 4:
            return
        self.zoom_ratio *= 1.1
        if self.zoom_ratio > 4:
            self.zoom_ratio = 4
        self._canvas_redraw()

    def zoom_out(self, event=None):
        if self._pil_img is None or self.zoom_ratio == 0.5:
            return
        self.zoom_ratio *= 0.9
        if self.zoom_ratio < 0.5:
            self.zoom_ratio = 0.5
        self._canvas_redraw()

    def zoom_reset(self, event=None):
        if self._pil_img is None or self.zoom_ratio == 1:
            return
        self.zoom_ratio = 1
        self._canvas_redraw()

    # --- scale
    def _scale_cancel(self, event=None):
        """Abort scale definition."""
        self._scale_draw.clear()
        self.canvas.delete("tmp_scale")

    def _scale_start(self, event):
        if self._pil_img is None:
            return
        x, y = self.canvas.canvasx(event.x), self.canvas.canvasy(event.y)
        self._scale_draw.extend([x, y, x, y])
        self.canvas.create_line(*self._scale_draw, tags=["tmp", "tmp_scale"], fill="magenta", width=2, arrow="both")

    def _draw_scale(self, event):
        self._scale_draw[-2] = self.canvas.canvasx(event.x)
        self._scale_draw[-1] = self.canvas.canvasy(event.y)
        self.canvas.coords("tmp_scale", *self._scale_draw)

    def _end_set_scale(self, event):

        def validate(event=None):
            try:
                scaled_x0 = float(start.get())
                scaled_x1 = float(end.get())
            except ValueError:
                top.destroy()
                return

            ax = axis.get()

            if ax == "x":
                x0, x1 = scale[::2]
            else:
                x0, x1 = scale[1::2]
            a_x = (scaled_x1 - scaled_x0)/(x1 - x0)
            self._scale[ax] = [a_x, x0, scaled_x0]  # [a_x, x0, scaled_x0] with scaled_x = a_x(x - x0) + scaled_x0
            self._scale_lines[ax] = scale
            self.canvas.delete(f"{ax}scale")
            self.canvas.create_line(*scale_draw, tags=f"{ax}scale", fill="magenta", width=2, arrow="both")
            for coords in self._measures:
                x0, y0 = self.scaled_x(coords[0]), self.scaled_y(coords[1])
                x1, y1 = self.scaled_x(coords[2]), self.scaled_y(coords[3])
                l = np.sqrt((x1 - x0)**2 + (y1 - y0)**2)
                self.canvas.itemconfigure("txt_measure{},{},{},{}".format(*coords), text=f"{l:g}")
            top.destroy()

        scale_draw = self._scale_draw.copy()
        scale = [c/self._zoom_ratio for c in self._scale_draw]
        top = tk.Toplevel(self)
        top.transient(self)
        top.columnconfigure(0, weight=1)
        top.columnconfigure(1, weight=1)
        top.title(_("Set scale"))
        ttk.Label(top, text=_("Axis")).grid(row=0, column=0, sticky="e", padx=4, pady=4)
        ttk.Label(top, text=_("Start")).grid(row=1, column=0, sticky="e", padx=4, pady=4)
        ttk.Label(top, text=_("End")).grid(row=2, column=0, sticky="e", padx=4, pady=4)

        axis = tk.StringVar(top, "x")
        if abs(scale[2] - scale[0]) < abs(scale[3] - scale[1]):
            axis.set("y")
        faxis = ttk.Frame(top)
        ttk.Radiobutton(faxis, text="x", value="x", variable=axis).pack(side="left", padx=4)
        ttk.Radiobutton(faxis, text="y", value="y", variable=axis).pack(side="left", padx=4)
        start = ttk.Entry(top)
        end = ttk.Entry(top)
        faxis.grid(row=0, column=1, sticky="w", pady=4)
        start.grid(row=1, column=1, sticky="w", padx=4, pady=4)
        end.grid(row=2, column=1, sticky="w", padx=4, pady=4)
        bframe = ttk.Frame(top)
        bframe.grid(row=3, columnspan=2)
        ttk.Button(bframe, text=_("Ok"), command=validate).pack(side="left", padx=4, pady=4)
        ttk.Button(bframe, text=_("Cancel"), command=top.destroy).pack(side="right", padx=4, pady=4)

        start.bind("<Return>", validate)
        end.bind("<Return>", validate)
        start.focus_set()

        top.grab_set()
        self.wait_window(top)
        self._scale_cancel()

    # --- measures
    def measure_add(self, *coords):
        self._measures.append(coords)
        x0, y0 = self.scaled_x(coords[0]), self.scaled_y(coords[1])
        x1, y1 = self.scaled_x(coords[2]), self.scaled_y(coords[3])
        l = np.sqrt((x1 - x0)**2 + (y1 - y0)**2)

        if l == 0:
            return

        iid = "measure{},{},{},{}".format(*coords)
        cs = [c*self._zoom_ratio for c in coords]
        self.canvas.create_line(*cs, fill="cyan", arrow="both",
                                tags=["measures", iid, "line_" + iid])

        xt = (coords[0] + coords[2])/2*self._zoom_ratio
        yt = (coords[1] + coords[3])/2*self._zoom_ratio
        if abs(coords[0] - coords[2]) > abs(coords[1] - coords[3]):
            # ~ horizontal
            anchor = "s"
        else:  # ~ vertical
            anchor = "w"
        self.canvas.create_text(xt, yt, text=f"{l:g}", anchor=anchor, fill="cyan",
                                tags=["measures", iid, "txt_" + iid])
        return iid

    def _measure_start(self, event):
        x, y = self.canvas.canvasx(event.x), self.canvas.canvasy(event.y)
        self._measure_draw.extend([x, y, x, y])
        self.canvas.create_line(*self._measure_draw, tags=["tmp", "tmp_measure"], fill="cyan", width=2, arrow="both")

    def _measure_cancel(self, event=None):
        self._measure_draw.clear()
        self.canvas.delete("tmp_measure")

    # --- guides
    def _guide_redraw(self):
        """Redraw guides."""
        for y0 in self._hguides:
            y = y0 * self._zoom_ratio
            self.canvas.coords(f"hguide{y0}", [self._xlim[0], y, self._xlim[1], y])

        for x0 in self._vguides:
            x = x0 * self._zoom_ratio
            self.canvas.coords(f"vguide{x0}", [x, self._ylim[0], x, self._xlim[1]])

        mode = self._draw_mode.get()
        if mode == "hguide":
            y = self._mouse_pos[1]
            self.canvas.coords("tmp_guide", self._xlim[0], y, self._xlim[1], y)
        elif mode == "vguide":
            x = self._mouse_pos[1]
            self.canvas.coords("tmp_guide", x, self._ylim[0], x, self._ylim[1])

    def hguide_add(self, y0):
         self._hguides.append(y0)
         iid = f"hguide{y0}"
         y = y0*self.zoom_ratio
         self.canvas.create_line(self._xlim[0], y, self._xlim[1], y, fill="blue", tags=["guides", iid])
         return iid

    def vguide_add(self, x0):
        self._vguides.append(x0)
        iid = f"vguide{x0}"
        x = x0 * self._zoom_ratio
        self.canvas.create_line(x, self._ylim[0], x, self._ylim[1], fill="blue", tags=["guides", iid])
        return iid

    def _resize(self, event):
        try:
            x0, y0, x1, y1 = self.canvas.bbox("image")
        except TypeError:
            x0, y0, x1, y1 = 0, 0, 1, 1
        self._xlim = [min(x0, 0), max(x1, event.width)]
        self._ylim = [min(y0, 0), max(y1, event.height)]

        self._guide_redraw()
